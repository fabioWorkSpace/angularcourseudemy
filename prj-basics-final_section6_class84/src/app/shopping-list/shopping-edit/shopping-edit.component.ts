import { Component, OnInit, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild('nameInput', {static: false}) nameInputRef: ElementRef; // local reference nameInput from this template #nameInput
  @ViewChild('amountInput', {static: false}) amountInputRef: ElementRef; // local reference amountInput from this template #amountInput
  
  @Output() ingredientAdded = new EventEmitter<{name: string, amount: number}>();
  // or
  // ingredientAdded = new EventEmitter<Ingredient>();
  // with the import from ingredient.model; ...


  constructor() { }

  ngOnInit() {
  }

  onAddItem() {
    const ingName = this.nameInputRef.nativeElement.value;
    const ingAmount = this.amountInputRef.nativeElement.value;
    const newIngredient = new Ingredient(ingName, ingAmount);
    this.ingredientAdded.emit(newIngredient);
  }

}
