import { Component, OnInit } from '@angular/core';
import { Recipe } from './recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  // receive this from this template (recipeWasSelected), which is from type Recipe
  selectedRecipe: Recipe;

  constructor() { }

  ngOnInit() {
  }

}
