import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  // emit the event 
  @Output() recipeWasSelected = new EventEmitter<Recipe>(); // pass the recipe, which is type Recipe
  
  recipes: Recipe[] = [
    new Recipe('A Test Recipe', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg'),
    new Recipe('Another Test Recipe', 'This is simply a test', 'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg')
  ];

  constructor() { }

  ngOnInit() {
  }

  // this method receives the (recipeEl) from this template, which is from the Recipe
  onRecipeSelected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe); // pass the recipe to the eventemitter recipeWasSelected
  }

}
